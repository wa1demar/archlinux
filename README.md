My Arch configs
==============

Description
-----------
* `zshrc` - settings (for root and normal user) for zsh. It is stored as `$HOME/.zshrc`;
* `vimrc` - settings for VIM. It is stored as `$HOME/.vimrc`;
* `sakura.conf` - settings for Sakura terminal. It is stored in `$HOME/.config/sakura/sakura.conf`;
* `local.conf` - custom settings for fontconfig. It is stored as `/etc/fonts/local.conf`
* `mc` custom setting for Midnight Commander. It is stored as `.config/mc/ini`;
